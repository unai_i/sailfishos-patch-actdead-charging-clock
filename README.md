Sailfish OS actdead charging  clock
===================================

This patch adds a clock to the charging screen when the device is off.
This allows to read the current time without having to power on the device.
.
