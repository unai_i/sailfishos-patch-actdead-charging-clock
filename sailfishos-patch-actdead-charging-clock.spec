Name:          sailfishos-patch-actdead-charging-clock
Version:       0.1.0
Release:       1
Summary:       Actdead charging clock
Group:         System/Tools
Vendor:        unai_i
Distribution:  SailfishOS
Requires:      sailfish-version >= 2.0.5, patchmanager
Packager:      unai_i <u.irigoyen@gmail.com>
URL:           apps.u-irigoyen.com
License:       GPL

%description
Add a clock to the charging screen when  the device is off.

%files
%defattr(-,root,root,-)
/usr/share/*

%preun
patchmanager -u sailfishos-actdead-charging-clock

%postun
if [ $1 = 0 ]; then
    // Do stuff specific to uninstalls
rm -rf /usr/share/patchmanager/patches/sailfishos-actdead-charging-clock
else
if [ $1 = 1 ]; then
    // Do stuff specific to upgrades
echo "Upgrading"
patchmanager -u sailfishos-actdead-charging-clock
fi
fi

%changelog
* Mon Dec 11 2017 0.1.0
- First buid..

